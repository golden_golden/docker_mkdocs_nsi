# Docker_mkdocs_build


<br>

## Description
Repositories pour construire les images docker utilisées dans mes projets mkdocs.
Le projet est scindée en deux partie.


### *L'images de production (utilisé par gitlab pour l'intégrassion continue)*

1. L'image utilisée est obtenue grace au dockerfile.
``` docker
    FROM python:alpine3.16


    # Build-time flags
    ARG WITH_PLUGINS=true

    # Environment variables
    ENV PACKAGES=/usr/local/lib/python3.9/site-packages
    ENV PYTHONDONTWRITEBYTECODE=1

    # Set build directory
    WORKDIR /tmp

    # Copy files necessary for build
    COPY requirements.txt .


    RUN apk update \
    && apk add --no-cache git \
    && pip install --upgrade pip \
    && pip install -r requirements.txt \
    && rm requirements.txt

    # Set working directory
    WORKDIR /docs
```

2. Le fichier `requirements.txt`
    - Permet de séléctionner les plugins à ajouter à python:
        - *selenium*
        - *mkdocs-material*
        - *mkdocs-macros-plugin*
        - *mkdocs-awesome-pages-plugin*
        - *mkdocs-exclude-search*
        - *mkdocs-kroki-plugin*
        - *git+https://github.com/Epithumia/mkdocs-sqlite-console.git*




### *L'images de développement (utilisé sur les postes perso pour la conception)*
1. Basé sur l'image de prod.
2. Ajout de l'ouverture du port 80 et de la partie serveur.
``` docker
    FROM goldengolden28/mkdocs-material-plugins-nsi-prod:latest

    # Expose MkDocs development server port
    EXPOSE 8000

    # Start development server by default
    ENTRYPOINT ["mkdocs"]
    CMD ["serve", "--dev-addr=0.0.0.0:8000"]
```

<br>

## Construction des images docker
*Il est absolument nécéssaire de construire l'image prod avant l'image dev !*

### *1. Image de prod*
1. Cloner en local.
2. Ouvrir dans un terminal
    - Rentrer la commande : `docker build -t goldengolden28/mkdocs-material-plugins-nsi-prod:2022.07 .`
    - **Remarque** : 
        - Le nom `goldengolden28/mkdocs-material-plugins-nsi-prod` dépend du repo présent sur docker hub
        - Le numéro de version est constitué de la date année.mois.
        - ne surtout pas oublier ` .` à la fin de la commande.
3. Changer le tag
    - Dans le terminal, utiliser la commande : `docker tag [id image goldengolden28/mkdocs-material-plugins-nsi-prod:2022.07] goldengolden28/mkdocs-material-plugins-nsi-prod:latest`
4. push sur *docker hub*.
    - Dans le terminal, utiliser la commande : `docker push goldengolden28/mkdocs-material-plugins-nsi-prod:latest`

### *2. Image de dev*
1. Cloner en local.
2. Ouvrir dans un terminal
    - Rentrer la commande : `docker build -t goldengolden28/mkdocs-material-plugins-nsi-dev:latest .`
    - **Remarque** : 
        - Le nom `goldengolden28/mkdocs-material-plugins-nsi-prod` dépend du repo présent sur docker hub
        - Le numéro de version est celui de l'image mkdocs-material qui a servit de base.
        - ne surtout pas oublier ` .` à la fin de la commande.

4. push sur *docker hub*
    - Dans le terminal, utiliser la commande : `docker push goldengolden28/mkdocs-material-plugins-nsi-dev:latest`

<br>

## Project status
Images en production, disponible sur *docker hub* : 
- [*goldengolden28/mkdocs-material-plugins-prod*](https://hub.docker.com/r/goldengolden28/mkdocs-material-plugins-prod)
- [*goldengolden28/mkdocs-material-plugins-dev*](https://hub.docker.com/r/goldengolden28/mkdocs-material-plugins-dev)


